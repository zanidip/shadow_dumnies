# shadowsocks for dummies

The goal of this tool is to allow everyone to create his own private VPN with :
 - this tutorial to create a server on google cloud in 10 minutes for free
 - A script that install the VPN in one copy-paste

## Caution :

 - Be carefull to not run more than one server on a long time on google cloud or you'll probably lock your free trial.

# HOW TO

### Register a google cloud account

Got to google cloud here : https://cloud.google.com

After accepting the licence, register a credit card to verify your identity. You'll not pay anything except if you manually chose to switch to a pay account.

### Create the server


- [Click here](https://console.cloud.google.com/compute/) to go to the compute engine instances page

- Clic `Create` to create the server

- change these 2 things as show bellow :
  - Boot disk : `Ubuntu 18.04 LTS`
  - Allow `HTTP` and `HTTPS` traffic

<img src="images/screen02.png" height="500" />

- then click `Create` again


### Run the VPN script

Once the server is created, you should see it in the compute engine page.

Click on the SSH connect to open a terminal :

<img src="images/screen03.png" height="180" />

In the terminal (new window), paste this entire command then press enter.

> `curl https://gitlab.com/zanidip/shadow_dumnies/raw/master/shadow_for_dumnies.sh > /tmp/script.sh && sudo bash /tmp/script.sh`


The script will :
- install the VPN
- ask you for a password
- Configure the stuff
- give you a recap with all the information you need to use the vpn :

```
### Installation succesfull. ###
You can now configure your shadowsock app with these manual settings :
- Server         : 35.122.190.16
- Remote Port    : 443
- Password       : the one you just set up
- Encrypt Method : aes-256-cfb

Or copy this url and import it in your shadowsock client :
    ss://YWVpLTI1NilkjLig9dA==@35.192.90.161:443
```

You can then use the url or the settings to configure your shadowsock client and use the VPN.

### Using the VPN

You can find clients for every device (smartphone or computer) [here](https://shadowsocks.org/en/download/clients.html)

# Notes

Works on :

| OS | Status |
|:----------|:--:|
| Debian 9  | :x: |
| Debian 10 | :x: |
| Ubuntu 16 | :x: |
| Ubuntu 18 | :heavy_check_mark: |
| Ubuntu 19 | :heavy_check_mark: |
