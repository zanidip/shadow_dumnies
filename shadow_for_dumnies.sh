
#!/bin/bash

ENCRYPT_METHOD=aes-256-cfb
PORT=443

apt-get update -q
DEBIAN_FRONTEND=noninteractive apt-get -y upgrade -q
apt-get install python-pip -y

pip install shadowsocks

sed -i 's/cleanup/reset/g' /usr/local/lib/python2.7/dist-packages/shadowsocks/crypto/openssl.py

while true;
    do
    echo "please enter a password (that you don't use somewhere else) :"
    read -sp "Password : " password
    echo "" # newline
    read -sp "Confirm  : " password2
    [[ "$password" == "$password2" ]] && break
    echo -e '\npasswords do not match'
done
cat <<EOT > /etc/shadowsocks.json
{
"local_address": "127.0.0.1",
"local_port":1080,
"server_port":"$PORT",
"password":"$password",
"timeout":600,
"method":"$ENCRYPT_METHOD"
}
EOT

echo "running shadowsocks"

ssserver -c /etc/shadowsocks.json -d start

server_ip=`curl -s ifconfig.me`

cat <<EOF

### Installation succesfull. ###

You can now configure your shadowsock app with these manual settings :
- Server         : $server_ip
- Remote Port    : $PORT
- Password       : the one you just set up
- Encrypt Method : $ENCRYPT_METHOD

EOF

# This one can crash on certain password
encrypted_url="`echo -n "$ENCRYPT_METHOD:$password" | base64`@$server_ip:$PORT" || exit

cat <<EOF

Or copy this url and import it in your shadowsock client : (may not works with complex passwords) :
    ss://$encrypted_url

EOF
